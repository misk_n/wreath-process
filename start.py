import numpy as np
from PIL import Image


# Three dimensional points
# Numpy matrices are not hashable so we need a wrapper class in order to insert in a set
# The third dimension is there for matrix operations. It should always be set to 1.
class Point:
    def __init__(self, x=0, y=0, matrix=None):
        if matrix is None:
            self.point = np.ones((3, 1))
            self.point[0, 0] = x
            self.point[1, 0] = y
        else:
            self.point = matrix


# Base virtual class
class Group:
    def __init__(self, group_set):
        self.set = group_set

    def apply(self, points):
        pass


# Reflection/Mirroring group
class Mirror(Group):
    def __init__(self, group_set, height=20, width=20, axis=0):
        Group.__init__(self, group_set)
        self.axis=axis
        self.height = height - 1
        self.width = width - 1

    def apply(self, points):
        res = set()
        for k in self.set:
            for x in points:
                matrix = np.ones((3, 1))
                matrix[0][0] = x.point[0, 0] + ((self.width / 2) - x.point[0,0]) * 2 * (k%2) * (1 - self.axis)
                matrix[1][0] = x.point[1, 0] + ((self.height / 2) - x.point[1,0]) * 2 * (k%2) * self.axis
                res.add(Point(matrix=matrix))
        return res



# Translation group
class Translation(Group):
    def __init__(self, group_set, axis=0):
        Group.__init__(self, group_set)
        self.axis = axis

    def apply(self, points):
        res = set()
        for t in self.set:
            for x in points:
                matrix = np.identity(3)
                matrix[0, 2] = (1 - self.axis) * t
                matrix[1, 2] = self.axis * t
                res.add(Point(matrix=(matrix @ x.point)))
        return res

# Rotation group
class Rotation(Group):
    def __init__(self, group_set, origin=0):
        Group.__init__(self, group_set)
        self.origin = origin

    def apply(self, points):
        # Use origin?
        res = set()
        for k in self.set:
            n = len(self.set)
            theta = 2 * np.pi * k / n
            #print(theta)
            matrix = np.identity(3)
            matrix[0, 0] = np.cos(theta)
            matrix[0, 1] = np.sin(theta)
            matrix[1, 0] = -np.sin(theta)
            matrix[1, 1] = np.cos(theta)
            #print(matrix)

            for x in points:
                res.add(Point(matrix=(matrix @ x.point)))
        return res

# Output matrix to compute distances between generated images
def output_matrix(points, height=20, width=20):
    output = np.zeros((height, width))
    for x in points:
        pos_x = int(x.point[0, 0])
        pos_y = int(x.point[1, 0])
        if 0 <= pos_x < width and 0 <= pos_y < height:
            output[pos_x, pos_y] = 1
    return output


# Helper function to show generated image
def generate_image(matrix, filename=None):
    height, width = matrix.shape
    image = np.ones((height, width, 3), dtype=np.uint8)
    for i in range(3):
        image[:,:, i] = image[:,:, i] - matrix
    image = 255 * image
    out = Image.fromarray(image, 'RGB')
    out=out.resize((out.size[0]*30,out.size[1]*30))
    if filename is not None:
        out.save(filename)
    else:
        out.show()


def wreath_product(groups, height=20, width=20):
    start = Point(1, 1)
    points = { start }
    for group in groups:
        points = points.union(group.apply(points))
    return points

def main():
    groups = [Translation({ 2 }), Mirror({ 1 }, axis=0), Rotation({ np.pi*5/4 })]
    points = wreath_product(groups)
    generate_image(output_matrix(points))

if __name__ == '__main__':
    main()
