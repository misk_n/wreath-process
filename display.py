import start 
import sys

# Display each picture corresponding to each action saved in a file
def display():
    groups = []
    points = start.wreath_product(groups)
    start.generate_image(start.output_matrix(points))
    fp = open(sys.argv[1])
    line = fp.readline()
    while line != '':
        input("Press Enter to continue...")
        line = line.split(' ')
        if line[0] == 'Translation':
            groups.append(start.Translation({int(line[1])}))
        elif line[0] == 'Rotation':
            groups.append(start.Rotation({float(line[1])}))
        elif line[0] == 'Mirror':
            groups.append(start.Mirror({int(line[1])}, axis=int(line[2])))
        points = start.wreath_product(groups)
        start.generate_image(start.output_matrix(points))
        line = fp.readline()


if __name__ == '__main__':
    display()
